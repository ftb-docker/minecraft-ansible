from datetime import datetime, timedelta
import logging
import os
import sys
import tarfile

from keystoneauth1.identity import generic
from keystoneauth1 import session as ksession

from openstack import connection
from openstack.exceptions import NotFoundException


log = logging.getLogger("backups")
log.propagate = False
file_hdlr = logging.StreamHandler()
formatter = logging.Formatter(
    '(%(asctime)s) - %(levelname)s - %(message)s')
file_hdlr.setFormatter(formatter)
log.addHandler(file_hdlr)
log.setLevel(logging.INFO)

BACKUP_DIRS = os.environ.get('BACKUPS_FOLDERS').split(',')
CONTAINER_NAME = os.environ.get('BACKUPS_SWIFT_CONTAINER')
DELETE_BACKUPS_AFTER = int(os.environ.get('BACKUPS_DELETE_AFTER'))

SWIFT_USERNAME = os.environ.get('BACKUPS_SWIFT_USERNAME')
SWIFT_PASSWORD = os.environ.get('BACKUPS_SWIFT_PASSWORD')
SWIFT_AUTH_URL = os.environ.get('BACKUPS_SWIFT_AUTH_URL')
SWIFT_PROJECT_NAME = os.environ.get('BACKUPS_SWIFT_PROJECT_NAME')
SWIFT_DOMAIN_NAME = os.environ.get('BACKUPS_SWIFT_DOMAIN_NAME')
SWIFT_REGION_NAME = os.environ.get('BACKUPS_SWIFT_REGION_NAME')


now = datetime.utcnow()
delete_after = now - timedelta(days=DELETE_BACKUPS_AFTER)

try:
    kwargs = {
            'username': SWIFT_USERNAME,
            'password': SWIFT_PASSWORD,
            'auth_url': SWIFT_AUTH_URL,
            'project_name': SWIFT_PROJECT_NAME,
            'project_domain_name': SWIFT_DOMAIN_NAME,
            'user_domain_name': SWIFT_DOMAIN_NAME,
        }
    auth = generic.Password(**kwargs)
    session = ksession.Session(auth=auth)
    conn = connection.Connection(
        session=session, region_name=SWIFT_REGION_NAME)
except Exception:
    log.exception("Error during auth and sdk setup.")
    sys.exit(1)


try:
    conn.object_store.get_container_metadata(CONTAINER_NAME)

    objects = list(conn.object_store.objects(CONTAINER_NAME))
    log.info("container %s exists." % CONTAINER_NAME)

    for obj in objects:
        last_modified = datetime.strptime(
            obj.last_modified_at, "%Y-%m-%dT%H:%M:%S.%f")
        if last_modified < delete_after:
            try:
                obj.delete(conn.object_store)
                log.info("Object %s deleted." % object['name'])
            except NotFoundException:
                log.info("Object %s deleted with 404." % object['name'])
except NotFoundException:
    conn.object_store.create_container(CONTAINER_NAME)
    log.info("container %s created." % CONTAINER_NAME)
except Exception:
    log.exception("Error during container check/create, or delete of expired.")
    sys.exit(1)

log.info("Making tarfile.")
tar_file_name = "backup_%s.tar.gz" % datetime.strftime(now, "%Y_%m_%d")
tar_file_location = "/tmp/%s" % tar_file_name
with tarfile.open(tar_file_location, "w:gz") as tar:
    for directory in BACKUP_DIRS:
        tar.add(directory)

log.info("Uploading backup.")
try:
    conn.create_object(
        container=CONTAINER_NAME,
        name=tar_file_name,
        filename=tar_file_location,
    )
    os.remove(tar_file_location)
    log.info("Uploaded %s" % tar_file_name)
except Exception:
    os.remove(tar_file_location)
    log.exception("Error during upload of minecraft backups.")
    sys.exit(1)
