# minecraft settings:

# minecraft version
minecraft_docker_image: registry.gitlab.com/ftb-docker/ftb-revelation
# you may want to pin this to a specific version
minecraft_docker_tag: latest

# max memory that java is allowed to use. This should be 80-90% of the host
# RAM:
minecraft_max_ram: 4096M
minecraft_max_players: 20
minecraft_whitelist_enabled: true

# The Minecraft server MOTD as seen in the server list:
minecraft_server_motd: "A Minecraft Server running ftb revelation {{ minecraft_docker_tag }}"
# The MOTD printed into chat when you login:
minecraft_login_motd:
  - Welcome!
  - This is a multi-line login MOTD!

# The default ops to always ensure are present (see vault file)
minecraft_default_ops: "{{ vault_minecraft_default_ops }}"

# Control to expose RCON from outside of the host.
# rcon is unsafe as hell to expose, be VERY careful with this.
minecraft_expose_rcon_on_host: false
minecraft_rcon_password: "{{ vault_minecraft_rcon_password }}"

# extra mod config files to add or override
# dest path is added to the mod config path
# configure_mods:
#   - src: files/example_mod.cfg
#     dest: example_mod.cfg

# Additional mods to drop in or override.
# dest path is added to the mod path
# additional_mods:
#   - src: files/vivecraft.jar
#     dest: vivecraft.jar

# if true will get HTTPS certs for domains. required DNS setup.
traefik_use_tls: true

# use testing server for letsencrypt when testing
# this should be false for sites you want to have 'real' certs, but is
# useful for testing when rebuilding often
traefik_acme_staging: false

# The email for letsencrypt certs
traefik_acme_email: "{{ vault_traefik_acme_email }}"

# domains to serve minedash on
minedash_domain_names: "{{ vault_minedash_domain_names }}"

# The name you want to give the website e.g. "My Awesome Minecraft Server"
minedash_django_wagtail_site_name: "MineDash"
# the base URL this site will end up at (needed for Wagtail admin)
minedash_django_base_url: "https//my-minecraft-example.com"

# minedash secrets
# Django debug can be useful to find errors with the site
minedash_django_debug: "false"
minedash_django_secret_key: "{{ vault_minedash_django_secret_key }}"
minedash_django_allowed_hosts: '{{ minedash_domain_names|join(", ") }}'

# default MineDash admin user
minedash_default_admin_username: "{{ vault_minedash_default_admin_username }}"
minedash_default_admin_email: "{{ vault_minedash_default_admin_email }}"
minedash_default_admin_password: "{{ vault_minedash_default_admin_password }}"

# internal db password
# this is mostly fine as is since the db isn't exposed, but better to be safe
minedash_django_database_password: "{{ vault_minedash_django_database_password }}"

# email settings
# if you want the website to be able to sent emails
# https://docs.djangoproject.com/en/2.0/ref/settings/#email-backend
minedash_django_email_backend: "django.core.mail.backends.console.EmailBackend"
minedash_django_email_host: "{{ vault_minedash_django_email_host }}"
minedash_django_email_host_password: "{{ vault_minedash_django_email_host_password }}"
minedash_django_email_host_user: "{{ vault_minedash_django_email_host_user }}"
minedash_django_email_port: "25"
minedash_django_email_use_tls: "False"
minedash_django_email_use_ssl: "False"
minedash_django_default_from_email: "{{ vault_minedash_django_default_from_email }}"

# date settings
minedash_django_time_zone: "Pacific/Auckland"
minedash_django_date_format: "j F, Y"
minedash_django_datetime_format: "j F, Y, P"

# Static file config
# by default the site will server static files from nginx, but a better
# approach is to use an object storage service.
# https://github.com/dennisv/django-storage-swift
minedash_django_default_file_storage: "django.core.files.storage.FileSystemStorage"
minedash_django_staticfiles_storage: "django.contrib.staticfiles.storage.StaticFilesStorage"
minedash_django_compress_storage: "compressor.storage.CompressorFileStorage"
minedash_django_swift_username: "{{ vault_minedash_django_swift_username }}"
minedash_django_swift_password: "{{ vault_minedash_django_swift_password }}"
minedash_django_swift_auth_url: "{{ vault_minedash_django_swift_auth_url }}"
minedash_django_swift_project_name: "{{ vault_minedash_django_swift_project_name }}"
minedash_django_swift_user_domain_name: "Default"
minedash_django_swift_project_domain_name: "Default"
minedash_django_swift_region_name: "{{ vault_minedash_django_swift_region_name }}"
minedash_django_swift_container_name: "minedash_django_storage"
minedash_django_swift_static_container_name: "minedash_django_static"
minedash_django_swift_auto_create_container: "True"
minedash_django_swift_auto_create_container_public: "True"
minedash_django_swift_auto_create_container_allow_origin: "*"

# google settings:
minedash_django_recaptcha_public_key: "{{ vault_minedash_django_recaptcha_public_key }}"
minedash_django_recaptcha_private_key: "{{ vault_minedash_django_recaptcha_private_key }}"
minedash_django_google_analytics_tracking_id: "{{ vault_minedash_django_google_analytics_tracking_id }}"

# robots.txt setting, if true overrides all rules (useful for dev)
# or when you don't want the site crawled at all
minedash_django_robots_disallow_all: "False"


# backup settings:
backup_server: False

# In days
backups_delete_after: 30

backups_swift_container: "minecraft-docker-backups"
backups_swift_username: "{{ vault_backups_swift_username }}"
backups_swift_password: "{{ vault_backups_swift_password }}"
backups_swift_auth_url: "{{ vault_backups_swift_auth_url }}"
backups_swift_project_name: "{{ vault_backups_swift_project_name }}"
backups_swift_domain_name: "Default"
backups_swift_region_name: "{{ vault_backups_swift_region_name }}"
