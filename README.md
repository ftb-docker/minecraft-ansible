# minecraft-ansible

A template project for deploying and managing a Minecraft server with an FTB
modpack using Ansible and Docker on CoreOS.

This will deploy a server (or multiple servers), with a given FTB modpack
container, and [Minedash](https://gitlab.com/uncaught-exceptions/minedash).

## Prerequisites

This guide assumes that you have a working Linux (or unix like) environment, if
you don't, then you'll need to find similar equivalents yourself for your OS.

This guide also assumes a certain amount of familiarity with basic web
infrastructure deployment. There is an assumption you know about SSH, DNS, and
can use a cloud provider to launch a Virtual Machine for your server. Knowledge
of Docker and Ansible is useful, but you can learn that as you go, or from
using this.

You may want to read through the full README before doing the prerequisites to
make sure you understand what all this entails.

### Minecraft CI ssh key

The first thing you need is an ssh key which we can use for your gitlab ci:
```
ssh-keygen -t rsa -b 4096 -N '' -f .ssh/minecraft_ci.rsa
```
This will create two files in your `.ssh` folder:
```
minecraft_ci.rsa
minecraft_ci.rsa.pub
```
The first is your private key, while the second (ending in `.pub`) is your
public key.

You'll need these later, but you shouldn't use these for SSH yourself. These
are purely for your CI system, and later you'll have your CI system inject your
personal SSH keys onto the hosts.

### A CoreOS host

This guide is built entirely on the assumption that you will use a CoreOS host.
If you want to use something else you will need to fork and rewrite large parts
of the Ansible.

What you need is a cloud provider where you can launch a CoreOS host and into
that host inject the SSH public key we created in the above step
(`minecraft_ci.rsa.pub`). Most cloud providers have mechanisms for injecting
ssh keys on boot, and you should avoid password auth with SSH like the plague
(it's bad and unsafe).

You will need to make sure the host is accessible with a public internet facing
ip address. Most cloud providers also have some sort of basic firewalling and
you should use that to limit ingress traffic to only ports 22, 80, 443, 25565,
ICMP (ping). You don't have to limit egress from the host.


The benefit of an OS like CoreOS is that it is a near empty shell, the OS can't
run anything itself directly, everything has to be in Docker. Even the scripts
that this Ansible project uses has scripts running in Docker. This means there
are less attack vectors so it is easier to secure and less effort to
maintain. This project even includes a very easy `patch` action that will
update and reboot the host.

### DNS pointing to your host (semi-optional)

While you can run this all without DNS (a domain name) pointing to your server
that will mean you can't do HTTPS and your Minedash website will not be exposed
securely to the web. You can use IP addressed entirely, but don't, get a domain
and point your domain with DNS records to the IP address of the host.

## How to use this repo

Now that you have your SSH key, your CoreOS Host, and DNS pointing to your host
it's time to start looking at using this guide.

First, fork it on gitlab.com, then clone your local fork to your computer.


### Hosts files

Now you need to find `inventory/hosts.yml`. This file tells Ansible what
servers (hosts) to talk to. It looks like this:
```
all:
  children:
    coreos:
      children:
        minecraft:
          hosts:
            # your server domain names:
            my-minecraft-example.com:
```
You should replace `my-minecraft-example.com` with your domain. E.g. if your
domain is `mc-awesome.com`, then the follow content should look like this:
```
all:
  children:
    coreos:
      children:
        minecraft:
          hosts:
            # your server domain names:
            mc-awesome.com:
```
And if you have more than one domain you want this Ansible to deploy to, this
is where you add it as well:
```
all:
  children:
    coreos:
      children:
        minecraft:
          hosts:
            # your server domain names:
            mc-awesome.com:
            mc-awesome-two.com:
            mc-awesome-three.com:
```
For a better understanding of what this actually means, take a look at the
Ansible docs: http://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html

### Host vars

Next find the folder `inventory/host_vars/my-minecraft-example.com` and rename
it to your host. E.g. if your domain is `mc-awesome.com` that folder
should be `inventory/host_vars/mc-awesome.com`. This folder represents the
configuration values specific to that host.

If you have more than one host you what this Ansible to deploy then you want to
duplicate that folder and give it the name of the other host. E.g. your
host_vars folder would look like:
```
inventory/host_vars/mc-awesome.com
inventory/host_vars/mc-awesome-two.com
inventory/host_vars/mc-awesome-three.com
```
Inside each of those folders is a `var.yml` file and a `vault.yml` file. These
files allow you to control host specific values, so if you want different
passwords or even different mod packs deployed on each host, this is how you do
it.

### Ansible Vault

Now that you have your hosts setup, it's time to start looking at the all the
configuration information. You'll need Ansible installed on your system, figure
that part out yourself.

First, we need to decrypt the vault files, there are two example ones:
```
inventory/group_vars/all/vault_users.yml
inventory/host_vars/my-minecraft-example.com/vault.yml
```
The latter of them likely now in a folder that matches your host name (e.g.
`inventory/host_vars/mc-awesome.com/vault.yml`).

To decrypt them you'll want to run `ansible-vault decrypt` with a path to each
of the vault files, e.g.:
```
ansible-vault decrypt inventory/group_vars/all/vault_users.yml inventory/host_vars/<your_host_name>/vault.yml
```
The password for the example vault files is:
```
Whats_with_all_the_terrible_passwords_in_this_damn_repo_already
```
**IMPORTANT** Do not git commit these files decrypted, *ever*. When they are
encrypted committing them is fine because a strong key will keep the safe
enough.

If you happen to use Atom as your text editor, you can also use a plugin to
handle your encryption and decryption: https://atom.io/packages/ansible-vault
This is probably a similar plugin for Sublime or other editors.

You can encrypt them with the `ansible-vault encrypt` command and pointing to
the vault files.

### Configuration

Most of the configuration for this repo is handled with the files in
`inventory/host_vars`. The general stuff that doesn't need to be a secret goes
in `vars.yml` for your particular host, while the secrets go into `vault.yml`.

Both files have reasonably ok comments around each variable or variable group
or the meaning of the variable can be gleamed from the name.

There are additional config options which can be set in `inventory/group_vars`
such as in `inventory/group_vars/all` where users vault file is. This file
right now is setup so any users and ssh public keys placed in that file will
be added to all hosts. You can also add a simlar var into the per host values
in `inventory/host_vars`, but it is assumed the same people will manage all the
hosts managed by this project.

#### Generating passwords

Most of the passwords in your vault are not ones you ever need to use directly
except maybe the default admin user.

For all the rest, you should generate your own with something like the
following command:
```
python -c "import random; print(''.join([random.SystemRandom().choice('abcdefghijklmnopqrstuvwxyz0123456789@#$%^&*(-_=+)') for i in range(50)]))"
```

#### Minecraft configuration

These config values in `vars.yml` and `vault.yml` should be fairly self
explanatory, but one that is a little unclear is `minecraft_docker_image` and
`minecraft_docker_tag`. These refer to prebuilt Docker images built for a given
modpack as coming from: https://gitlab.com/ftb-docker

The default is [FTB Revalation](https://gitlab.com/ftb-docker/ftb-revelation),
and the tag `latest`, although this tag can be changed to a specific version if
needed. If left on latest your server will jump to any new versions if they
exist whenever you trigger a release action.

These images are containers build around the FTB modpacks for Minecraft 1.12
and they rebuild daily so they are always up to date with Java and the
underlying operating system. While this Ansible can be easily changed to work
with other modpacks, the assumption for now is these specific FTB modpacks, but
we can easily add other ones later. The main assumption this Ansible project
makes is the presence of FTBUtils for backup and rank support.

#### Minedash configuration

[Minedash](https://gitlab.com/uncaught-exceptions/minedash) is an application
built on [Wagtail](https://wagtail.io/), which in turn is built on
[Django](https://www.djangoproject.com/).

The main places to look and try and understand some of these setting may be:
https://gitlab.com/uncaught-exceptions/minedash/blob/master/minedash_app/minedash/settings/config.py
https://docs.djangoproject.com/en/2.0/topics/settings/
http://docs.wagtail.io/en/v2.0.1/reference/contrib/settings.html

Look through `vars.yml` and `vault.yml` and look at the variables labeled with
`minedash`.

The main one you'll need to look at is `vault_minedash_domain_names`. These are
the domain names the website will be served on, and what domains Traefik will
get HTTPS certs for. Make sure you have DNS setup for these. An example:
```
vault_minedash_domain_names:
  - my-minecraft-example.com
  - www.my-minecraft-example.com
```
**Note**: the `www` subdomain will automatically redirect to the main domain
if you set a www subdomain. This is because the `www` subdomain is stupid
and a relic of the past. Set it if you want, but the site will redirect away
from it.

##### Static file config

Static files, such as javascript, css, etc, are by default served from the
host itself. So are any media files uploaded to the website. If you plan to
use the wiki quite a bit, you probably want to use Object Storage to serve the
static and media files, as this means they aren't on the host disk.

Minedash uses the following library for that:
https://github.com/dennisv/django-storage-swift

In future I hope to include support for S3 as well.

#### Backups

The minecraft server backs up into `/docker/volumes/minecraft/backups` and the
website database backs up into `/docker/volumes/minedash/postgresql/backup`,
with the site media files going into `/docker/volumes/minedash/media/`.

Then the host has the option to take the entire `/docker/volumes` folder and
back it up into a GZIP'd tar file and upload that off the host into object
storage.

This is **very** useful. Turning this on is useful since if the host dies, you
have backups somewhere else, and they will delete anything older than a
configured.

Ideally I want to also add optional S3 support for this as well.

##### Restore backups

Right now this is a manual process, but I want to add this to the Ansible as
files you give it to restore.

What it amounts to is copy the tarzip file to the new host, unzip into the
`/docker/volumes` folder. You also may need to restore one of the Minecraft
backups from `/docker/volumes/minecraft/backups` into the
`/docker/volumes/minecraft/world` folder.

Mostly restoring the data in `/docker/volumes` should be enough.

**TODO**: Need to write example on how to load in a postgresql dump via a
docker container.

#### GitLab CI

So once you've done all the configuration you want and you've encrypted your
vault files, commit them and push them to your gitlab fork.

Now to actually use it, we're going to need to give Gitlab the ability to
decrypt your vault, and also ssh to your host.

In your gitlab repo you will want to add a secret deployment variable (via the
GUI) for `SSH_PRIVATE_KEY` and `ANSIBLE_VAULT_PHRASE`.

`SSH_PRIVATE_KEY` is the content of the private key we created earlier
(`.ssh/minecraft_ci.rsa`). While `ANSIBLE_VAULT_PHRASE` is the password you
used to encrypt your vault files.

With those two values in your GitLab CI secret variables, you can now run
pipelines on GitLab.


#### Run Ansible directly

While ideally you should run all of this from Gitlab as it's nicer and keeps
track of all the runs you do, and you can run it from a browser, you can
easily enough run it all directly:
```
ansible-playbook deploy-minecraft.yml -i inventory/ --ask-vault-pass
```
